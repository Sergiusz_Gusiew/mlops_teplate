# Project name

Description...

## Motivation


**Problem:**
...

**Solution:**
...

**Project constraints:**
- ...
-


## Method and results


First, introduce and motivate your chosen method, and explain how it contributes to solving the research question/business problem.

Second, summarize your results concisely. Make use of subheaders where appropriate.


## Repository overview

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

## Running instructions

Explain to potential users how to run/replicate your workflow. If necessary, touch upon the required input data, which secret credentials are required (and how to obtain them), which software tools are needed to run the workflow (including links to the installation instructions), and how to run the workflow.

**Stack**
1. Чистый Python 3.9.10, 
установка зависимостей: pip install -r requirements.txt,
обновление списка зависимостей: pip freeze -> requirements.txt
2. 

**Git**
0. GitHub Flow
1. GitLab
2. Обобые правила, установленные для проекта в GitLab:
- Settings - General - Merge requests - Merge checks - All discussions must be resolved
- Settings - Repository - Protected branches - branch: main, allowed to merge:
Developers + Maintainers, allowed to push: No one, allowed to force push: no

**Данные**
1. Исходные данные в data/raw.
2. 

**Артефакты разработки/src**
1. 

**Отчеты/notebooks**
1. 

**Документирование**
1. README.md - основная информация о проекте, правила заполнения находятся в 
references/README.template.md
2. CHANGELOG.md - лог изменений в проекте
3. Документирование событий Scrum - в Wiki на GitLab

## More resources

Point interested users to any related literature and/or documentation.

1. В разделе 'references' на текущий момент хранится шаблоны для проектного
документирования

2. По следующей ссылке описано соглашение, раскрывающее идеи, реализованные
в структуре данного проекта: https://drivendata.github.io/cookiecutter-data-science/

А также гайд по установке шаблона:
https://medium.com/analytics-vidhya/folder-structure-for-machine-learning-projects-a7e451a8caaa

## About

Explain who has contributed to the repository. You can say it has been part of a class you've taken at Tilburg University.

